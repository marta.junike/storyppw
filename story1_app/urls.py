from django.urls import path

from .views import story1
app_name = 'story1_app'

urlpatterns = [
    path('', story1, name='story1'),
    
]
