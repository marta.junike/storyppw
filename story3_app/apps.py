from django.apps import AppConfig

class MainConfig(AppConfig):
    name = 'story3_app'
