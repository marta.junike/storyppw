from django.urls import path

from .views import profile1,profile2,portofolio,comingsoon

app_name = 'story3_app'

urlpatterns = [
    path('', profile1, name='profile1'),
    path('profile2', profile2, name='profile2'),
    path('portofolio', portofolio, name='portofolio'),
    path('comingsoon', comingsoon, name='comingsoon'),
]
