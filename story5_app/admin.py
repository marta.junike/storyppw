from django.contrib import admin
from .models import mataKuliah

# Register your models here.

admin.site.register(mataKuliah)

class adminMatkul(admin.ModelAdmin):
    list_display = ('matakuliah', 'jumlah_sks', 'namaDosen', 'semester', 'ruangKelas', 'deskripsi')
    search_fields = ('matakuliah', 'jumlah_sks', 'namaDosen', 'semester', 'ruangKelas')

