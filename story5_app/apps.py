from django.apps import AppConfig


class Story5AppConfig(AppConfig):
    name = 'story5_app'
