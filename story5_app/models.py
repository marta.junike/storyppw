from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class mataKuliah (models.Model):
    SEM_CHOICE = [
        ("Gasal 2019/2020","Gasal 2019/2020"),
        ("Genap 2019/2020","Genap 2019/2020"),
        ("Gasal 2020/2021","Gasal 2020/2021"),
        ("Genap 2020/2021","Genap 2020/2021"),
        ("Others","Others")
    ]
    matakuliah = models.CharField("Mata Kuliah ",max_length=120,null=True)
    jumlah_sks = models.PositiveSmallIntegerField("Jumlah SKS ",default = 1, validators=[MaxValueValidator(24),MinValueValidator(1)])
    namaDosen = models.CharField("Nama Dosen ",max_length=120)
    semester = models.CharField("Semester ",max_length=120,choices=SEM_CHOICE,null=True, default=("Gasal 2019/2020","Gasal 2019/2020"))
    ruangKelas = models.CharField("Ruang Kelas ", max_length=120, default = "Online Class")
    deskripsi= models.TextField("Deskripsi Mata Kuliah ",max_length=120,null=True, default="-")

    def __str__(self):
        return f'{self.matakuliah}'