from django.urls import path

from .views import tambah_matkul,matkul,detail_matkul

app_name = 'story5_app'

urlpatterns = [
    path('add-matkul/', tambah_matkul, name='tambah_matkul'),
    path('', matkul, name='daftar_matkul'),
    path('<int:id>/', detail_matkul, name='detail_matkul'),
]
