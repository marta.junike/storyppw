from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import matkulForm
from .models import mataKuliah

def matkul(request):
    if request.method == 'POST':
        mataKuliah.objects.get(id = request.POST['id']).delete()
    daftarMatkul = mataKuliah.objects.all()    
    return render(request, 'matkul.html', {'daftarMatkul': daftarMatkul})

def tambah_matkul(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = matkulForm(request.POST or None)
        if form.is_valid():
            form.save()
            result = 1
        else:
            result = 0
        statusMatkul = mataKuliah.objects.all()
        return render(request, 'tambah_matkul.html', {'form': form,'database':statusMatkul,'result':result})
    else:
        form = matkulForm()
        statusMatkul = mataKuliah.objects.all()
        return render(request, 'tambah_matkul.html', {'form': form, 'database':statusMatkul})

def detail_matkul(request,id):
    daftarMatkul = mataKuliah.objects.get(pk = id)
    return render(request, 'detail_matkul.html', {'daftarMatkul': daftarMatkul})

