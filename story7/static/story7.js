$( document ).ready(function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        heightStyle: "content"
    });

    $('i.down').click(function(){
        var $parent = $(this).parents("li");
        $parent.insertAfter($parent.next());
        return false;
    });

    $('i.up').click(function(){
        var $parent = $(this).parents("li");
        $parent.insertBefore($parent.prev());
        return false;
    });

});