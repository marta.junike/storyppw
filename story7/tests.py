from django.test import TestCase, Client

# Create your tests here.

class TestStory7(TestCase):
    def test_story7_url_exist(self):
        response = Client().get("/story7")
        self.assertEquals(response.status_code, 200)

    def test_story7_template(self):
        response = Client().get("/story7")
        self.assertTemplateUsed(response,'story7.html')

    def test_accordion_exist(self):
        response = Client().get("/story7")
        htmlcontent = response.content.decode('utf8')
        self.assertIn('<div id="accordion">', htmlcontent)
