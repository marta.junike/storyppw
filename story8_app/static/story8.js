$(document).ready(()=>{
    $('#button').click( function(){
        var key = $('#search').val();

    $.ajax({
        method: 'GET',
        url: 'data?key=' + key +'/',
        success: function(response) {
            console.log(response)
            $('#data').empty();

            var value = "";
            for (i=0;i<response.items.length;i++){
                var title = response.items[i].volumeInfo.title;
                var author = response.items[i].volumeInfo.authors;
                value += ('<tr>' + '<td>' + title + '</td>' + '<td>' + author + '</td>')
                
                var image = response.items[i].volumeInfo.imageLinks;
                if (image != null) {
                    var imageSmall = response.items[i].volumeInfo.imageLinks.smallThumbnail;
                    value += ('<td><img src=' + imageSmall + '></td>' + '</tr>')
                } else {
                    value += ('<td> <p> No Image Preview Available </p> </td>'+ '</tr>')
                }
            }
            console.log(value);
            $('#data').append(value);
            $('#data').append('</table>');


        }

    })   
    })

});