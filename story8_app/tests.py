from django.test import TestCase, Client

# Create your tests here.

class UnitTestStory8(TestCase):
    def test_story8_url_exist(self):
        response = Client().get("/story8/")
        self.assertEquals(response.status_code, 200)

    def test_story8_template(self):
        response = Client().get("/story8/")
        self.assertTemplateUsed(response,'story8.html')

    def test_table_exist(self):
        response = Client().get("/story8/")
        htmlcontent = response.content.decode('utf8')
        self.assertIn('<input', htmlcontent)
        self.assertIn('Search', htmlcontent)
        self.assertIn('<table', htmlcontent)

    def test_url_getSearchData_is_exist(self):
        response = Client().get("/story8/data?key=cc")
        self.assertEquals(200, response.status_code)
