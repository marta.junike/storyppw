from django.urls import path

from . import views

app_name = 'story8_app'

urlpatterns = [
    path('', views.story8, name='story8'),
    path('data', views.getSearchData),
]
