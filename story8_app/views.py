from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
import requests
import json

# Create your views here.

def story8(request):
    return render(request,"story8.html")

def getSearchData(request):
    key = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)
