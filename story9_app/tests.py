from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.

class UnitTestStory9(TestCase):
    def test_login_url_exist(self):
        response = Client().get("/login")
        self.assertEquals(response.status_code, 200)

    def test_login_template_used(self):
        response = Client().get("/login")
        self.assertTemplateUsed(response,"login.html")

    def test_login_views_valid(self):
        self.credentials = {
            'username': 'dew',
            'password': 'kepo'}
        User.objects.create_user(**self.credentials)
        # send login data
        response = self.client.post('/login', self.credentials, follow=True)
        # should be logged in now
        self.assertTrue(response.context['user'].is_active)
    
    def test_register_url_exist(self):
        response = Client().get("/register")
        self.assertEquals(response.status_code, 200)
    
    def test_register_template_used(self):
        response = Client().get("/register")
        self.assertTemplateUsed(response,"regis.html")
    
    def test_hello_url_exist(self):
        response = Client().get("/hello")
        self.assertEquals(response.status_code, 200)
    
    def test_hello_template_used(self):
        response = Client().get("/hello")
        self.assertTemplateUsed(response,"hello.html")