from django.urls import path

from . import views

app_name = 'story9_app'

urlpatterns = [
    path('login', views.log_in, name='login'),
    path('logout', views.log_out, name='logout'),
    path('hello', views.hello, name='hello'),
    path('register', views.regis, name='register'),
]
