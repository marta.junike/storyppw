from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required


# Create your views here.
def log_in(request):
    if request.method == "POST":
        form = AuthenticationForm(request,data=request.POST)
        if form.is_valid():
            login(request,form.get_user())
            return redirect('/hello')
    else:
        form = AuthenticationForm()
    return render(request,"login.html",{'form': form})

def log_out(request):
    logout(request)
    return redirect('/')

def regis(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            saveData = form.save()
            login(request, saveData)
            return redirect('/hello')
    else:
        form = UserCreationForm()
    return render(request, "regis.html", {'form': form})

def hello(request):
    return render(request,"hello.html")
